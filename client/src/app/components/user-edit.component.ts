import { Component, OnInit } from '@angular/core';

import { GLOBAL } from '../services/global';
import '../services/user.service';
import '../models/user';
import { UserService } from '../services/user.service';
import { User } from '../models/user';

@Component({
    selector: 'user-edit',
    templateUrl: '../views/user-edit.html',
    providers: [UserService]
})

export class UserEditComponent implements OnInit {

    public title: string;
    public identity: User;
    public token: string;
    public user: User;
    public alertMessage: string;
    public url: string;

    constructor(
        private _userService: UserService
    ) {
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
        this.title = 'Actualizar mis datos';
        this.user = this.identity;
        this.url = GLOBAL.url;
    }

    ngOnInit(): void {
        console.log('Cargado el componente de edición de usuarios.');
    }

    onSubmit() {
        this._userService.updateUser(this.user).subscribe(
            response => {
                console.log(response);
                if (!(response as any).message) {
                    this.alertMessage = 'Error al actualizar el usuario.'
                }
                else {
                    localStorage.setItem('identity', JSON.stringify(this.user));
                    
                    if (this.filesToUpload) {
                        this
                            .makeFilesRequest(this.url + 'upload/user/image/' + this.user._id, null, this.filesToUpload)
                            .then(
                                (result: any) => {
                                    this.user.image = result.image;
                                    localStorage.setItem('identity', JSON.stringify(this.user));
                                    document.getElementById('userImage').setAttribute('src', this.url + 'user/image/' + this.user.image);
                                }
                            );
                    }

                    this.alertMessage = 'El usuario se ha actualizado correctamente';
                }
            },
            error => {
                this.alertMessage = error.error.message;

                if (this.alertMessage != null) {
                    console.log(this.alertMessage);
                }
            }
        );
    }

    public filesToUpload: Array<File>;

    fileChangeEvent(fileInput: any) {
        this.filesToUpload = <Array<File>>fileInput.target.files;
    }

    makeFilesRequest(
        url: string,
        params: Array<string>,
        files: Array<File>) {
        const token = this.token;

        return new Promise(function(resolve, reject) {
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();

            for (let i = 0; i < files.length; i++) {
                formData.append('image', files[i], files[i].name);
            }

            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            }

            xhr.open('POST', url, true);
            xhr.setRequestHeader('Authorization', token);
            xhr.send(formData);
        });
    }
}

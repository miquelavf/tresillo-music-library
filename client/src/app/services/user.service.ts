import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/Observable';
import { GLOBAL } from './global';
import { User } from '../models/user';

@Injectable()
export class UserService {
    public url: string;
    public identity: User;
    public token: string;

    constructor(private http: HttpClient) {
        this.url = GLOBAL.url;
    }

    signup(userToLogin, gethash = null) {
        if (gethash != null) { userToLogin.gethash = gethash; }

        const params = JSON.stringify(userToLogin);
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this.http.post(this.url + 'login', params, { headers });
    }

    getIdentity() {
        const identity = JSON.parse(localStorage.getItem('identity'));

        if (identity !== undefined) {
            this.identity = identity;
        } else {
            this.identity = null;
        }

        return this.identity;
    }

    getToken() {
        const token = localStorage.getItem('token');

        if (token !== undefined) {
            this.token = token;
        } else {
            this.token = null;
        }

        return this.token;
    }

    signIn(userToRegister: User) {
        const params = JSON.stringify(userToRegister);

        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this.http.post(this.url + 'register', params, { headers });
    }

    updateUser(userToUpdate: User) {
        const params = JSON.stringify(userToUpdate);

        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: this.getToken()
        });

        return this.http.put(this.url + 'user/update/' + userToUpdate._id, params, { headers });
    }
}

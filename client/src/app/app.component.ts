import { Component, OnInit } from '@angular/core';
import { UserService } from './services/user.service';
import { User } from './models/user';
import { GLOBAL } from './services/global';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [UserService]
})

export class AppComponent implements OnInit {

  public title = 'Tresillo';
  public user: User;
  public userToRegister: User;
  public identity: User;
  public token: string;
  public errorMessage;
  public url: string;
  private registrationSuccessful: boolean;

  constructor(
    private USERSERVICE: UserService
  ) {
    this.user = this.initializeUser();
    this.userToRegister = this.initializeUser();
    this.url = GLOBAL.url;
  }

  ngOnInit() {
    this.identity = this.USERSERVICE.getIdentity();
    this.token = this.USERSERVICE.getToken();
  }

  public onSubmit() { this.logUser(); }

  logUser() {
    this.USERSERVICE.signup(this.user).subscribe(
      response => {
        this.identity = (response as any).user;

        if (!this.identity._id) {
          alert('Login error.');
        } else {
          this.getUserToken();
        }
      },
      error => {
        this.errorMessage = error.error.message;

        if (this.errorMessage != null) {
          console.log(this.errorMessage);
        }
      }
    );
  }

  getUserToken() {
    if (this.identity == null) {
      return;
    }
    this.USERSERVICE.signup(this.user, true).subscribe(
      response => {
        this.token = (response as any).token;

        if (this.token.length <= 0) {
          alert('Token login error.');
        } else {
          localStorage.setItem('identity', JSON.stringify(this.identity));
          localStorage.setItem('token', this.token);
        }
      },
      error => {
        this.errorMessage = error.error.message;

        if (this.errorMessage != null) {
          console.log(this.errorMessage);
        }
      }
    );
  }

  logout() {
    localStorage.clear();
    location.reload();
  }

  signIn() {
    this.USERSERVICE.signIn(this.userToRegister).subscribe(
      response => {
        const user: User = (response as any).user;

        if (!user._id) {
          alert('Signup error.');
        } else {
          this.registrationSuccessful = true;
          this.userToRegister = this.initializeUser();
        }
      },
      error => {
        this.errorMessage = error.error.message;

        if (this.errorMessage != null) {
          console.log(this.errorMessage);
        }
      }
    )
  }

  initializeUser() { return new User('', '', '', '', '', 'ROLE_USER', ''); }
}

export class Song {
    constructor(
        public number: number,
        public title: string,
        public duration: number,
        public file: string,
        public album: string
        ) { }
}

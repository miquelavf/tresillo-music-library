export class Album {
    constructor(
        public title: string,
        public year: number,
        public description: string,
        public image: string,
        public artist: string
        ) { }
}

'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'tresillo-test-secret';

exports.ensureAuth = function(req, res, next) {
    if(!req.headers.authorization) {
        return res.status(403).send({ message: 'Missing authorization header.' })
    }

    var token = req.headers.authorization.replace(/['"]+/g, '');

    try {
        var payload = jwt.decode(token, secret);

        if(payload.exp <= moment.unix()) {
            return res.status(401).send({ message: 'Authentication token expired' });
        }
    } catch (error) {
        // eslint-disable-next-line no-console
        console.log(error);
        return res.status(404).send({ message: 'Authentication token not valid.' });
    }

    req.user = payload;

    next();
};
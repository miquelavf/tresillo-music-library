'use strict'

var mongoose = require('mongoose');
var app = require('./app');

var port = process.env.PORT || 3977;

mongoose.connect('mongodb://mongo:27017/tresillo', (err) => {
    if (err) {
        throw err;
    }
    else {
        console.log('La conexión a la base de datos está funcionando correctamente...');

        app.listen(port, () => {
            console.log('Servidor escuchando en puerto ' + port);
        });
    }
});
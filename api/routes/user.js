'use strict'

var express = require('express');
var userController = require('../controllers/user');

var api = express.Router();
var md_auth = require('../middleware/authentication');

var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads/users' });

api.post('/register', userController.saveUser);
api.post('/login', userController.loginUser);
api.put('/user/update/:id', md_auth.ensureAuth, userController.updateUser);
api.post('/upload/user/image/:id', [md_auth.ensureAuth, md_upload], userController.uploadImage);
api.get('/user/image/:imageFile', userController.getImageFile);

module.exports = api;
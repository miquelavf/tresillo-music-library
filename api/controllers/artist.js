'use strict'

require('path');
require('fs');
require("mongoose-pagination");

var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

function getArtist(req, res) {
    var aritstId = req.params.id;

    Artist.findById(aritstId, (err, artist) => {
        if (err) {
            return res.status(500).send({ message: 'Error en la petición.' });
        } else {
            if (!artist) {
                return res.status(404).send({ message: 'El artista no existe.' });
            } else {
                return res.status(200).send({ artist });
            }
        }
    });
}

function getArtists(req, res) {

    var page;

    if (req.params.page) {
        page = req.params.page;
    } else {
        page = 1;
    }
    var itemsPerPage = 3;

    Artist.find().sort('name').paginate(page, itemsPerPage, (err, artists, total) => {
        if (err) {
            return res.status(500).send({ message: 'Error en la petición' });
        } else {
            if (!artists) {
                return res.status(404).send({ message: 'No hay aritstas.' });
            } else {
                return res.status(200).send({
                    total_items: total,
                    aritsts: artists
                });
            }
        }
    });
}

function saveArtist(req, res) {
    var artist = new Artist();

    var params = req.body;
    artist.name = params.name;
    artist.description = params.description;
    artist.image = 'null';

    artist.save((err, artistStored) => {
        if (err) {
            return res.status(500).send({ message: 'Error al guardar el artista' });
        } else {
            if (!artistStored) {
                return res.status(404).send({ message: 'No se ha guardado el artista' });
            } else {
                return res.status(200).send({ artist: artistStored });
            }
        }
    });
}

function updateArtist(req, res) {
    var aritstId = req.params.id;
    var update = req.body;

    Artist.findByIdAndUpdate(aritstId, update, (err, artistUpdated) => {
        if (err) {
            return res.status(500).send({ message: 'Error al actualizar el artista' });
        } else {
            if (!artistUpdated) {
                return res.status(404).send({ message: 'No se ha encontrado el artista' });
            } else {
                return res.status(200).send({ artist: artistUpdated });
            }
        }
    });
}

function deleteArtist(req, res) {
    var artistId = req.params.id;

    Artist.findByIdAndRemove(artistId, (err, artistRemoved) => {
        if (err) {
            return res.status(500).send({ message: 'Error al borrar el artista' });
        } else {
            if (!artistRemoved) {
                return res.status(404).send({ message: 'No se ha encontrado el artista' });
            } else {
                Album.find({ artist: artistRemoved._id }).deleteMany((err, albumRemoved) =>
                {
                    if (err) {
                        return res.status(500).send({ message: 'Error al borrar el album' });
                    } else {
                        if (!albumRemoved) {
                            return res.status(404).send({ message: 'No se ha encontrado el album' });
                        } else {
                            Song.find({ album: albumRemoved._id }).deleteMany((err, songRemoved) =>{
                                if (err) {
                                    return res.status(500).send({ message: 'Error al borrar la cancion' });
                                } else {
                                    if (!songRemoved) {
                                        return res.status(404).send({ message: 'No se ha encontrado la cancion' });
                                    } else {
                                        res.status(200).send({ artist: artistRemoved })
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    });
}

function uploadImage(req, res) {
    var artistId = req.params.id;
    var file_name = 'Not uploaded';

    if (req.files) {
        var file_path = req.files.image.path;

        file_name = file_path.split(/(\\|\/)/g).pop();
        var file_ext = file_name.split('\.').pop();

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg') {
            Artist.findByIdAndUpdate(artistId, { image: file_name }, (err, artistUpdated) =>  {
                if (err) {
                    return res.status(500).send({ message: 'Error al actualizar el usuario.' });
                } else {
                    if (!artistUpdated) {
                        return res.status(404).send({ message: 'User not found.' });
                    } else {
                        return res.status(200).send({ message: artistUpdated });
                    }
                }
            });
        } else {
            return res.status(200).send('Invalid image format.');
        }

        console.log(file_name);
    } else {
        return res.status(200).send({ message: "Couldn't upload file." });
    }
}

function getImageFile(req, res) {
    var imageFile = req.params.imageFile;

    var filePath = './uploads/artists/' + imageFile;

    fs.exists(filePath, function(exists) {
        if (exists) {
            return res.sendFile(path.resolve(filePath));
        } else {
            return res.status(200).send({ message: "The image doesn't exist in the system." });
        }
    });
}

module.exports = {
    getArtist,
    saveArtist,
    getArtists,
    updateArtist,
    deleteArtist,
    uploadImage,
    getImageFile
}
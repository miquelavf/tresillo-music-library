'use strict'

require('path');
require('fs');
require("mongoose-pagination");

var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

function getAlbum(req, res) {
    var albumId = req.params.id;

    Album.findById(albumId).populate({ path: 'artist' }).exec((err, album) => {
        if (err) {
            return res.status(500).send({ message: 'Error en la petición.' });
        } else {
            if (!album) {
                return res.status(404).send({ message: 'El album no existe.' });
            } else {
                return res.status(200).send({ album: album });
            }
        }
    });
}

function getAlbums(req, res) {
    var artistId = req.params.artist;
    var found;

    if (!artistId) {
        found = Album.find().sort('title');
    } else {
        found = Album.find({ artist: artistId }).sort('year');
    }

    found.populate({ path: 'artist' }).exec((err, albums) => {
        if (err) {
            return res.status(500).send({ message: 'Error en la petición.' });
        } else {
            if (!albums) {
                return res.status(404).send({ message: 'No se han encontrado albums.' });
            } else {
                return res.status(200).send({ albums: albums });
            }
        }
    });
}

function saveAlbum(req, res) {
    var album = new Album();

    var params = req.body;

    album.title = params.title;
    album.description = params.description;
    album.year = params.year;
    album.image = 'null';
    album.artist = params.artist;

    album.save((err, albumStored) => {
        if (err) {
            return res.status(500).send({ message: 'Error en la petición.' })
        } else {
            if (!albumStored) {
                return res.status(404).send({ message: 'No se ha guardado el album' })
            } else {
                return res.status(200).send({ album: albumStored })
            }
        }
    });
}

function updateAlbum(req, res) {
    var albumId = req.params.id;
    var update = req.body;

    Album.findByIdAndUpdate(albumId, update, (err, albumUpdated) => {
        if (err) {
            return res.status(500).send({ message: 'Error en la petición.' })
        } else {
            if (!albumUpdated) {
                return res.status(404).send({ message: 'No se ha actualizado el album' })
            } else {
                return res.status(200).send({ album: albumUpdated })
            }
        }
    });
}

function deleteAlbum(req, res) {
    var albumId = req.params.id;

    Album.findByIdAndRemove(albumId, (err, albumRemoved) => {
        if (err) {
            return res.status(500).send({ message: 'Error al borrar el album' });
        } else {
            if (!albumRemoved) {
                return res.artistRemovedstatus(404).send({ message: 'No se ha encontrado el album' });
            } else {
                Song.find({ album: albumRemoved._id }).deleteMany((err, songRemoved) => {
                    if (err) {
                        return res.status(500).send({ message: 'Error al borrar la cancion' });
                    } else {
                        if (!songRemoved) {
                            return res.status(404).send({ message: 'No se ha encontrado la cancion' });
                        } else {
                            res.status(200).send({ album: albumRemoved })
                        }
                    }
                });
            }
        }
    });
}

function uploadImage(req, res) {
    var albumId = req.params.id;
    var file_name = 'Not uploaded';

    if (req.files) {
        var file_path = req.files.image.path;

        file_name = file_path.split(/(\\|\/)/g).pop();
        var file_ext = file_name.split('\.').pop();

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg') {
            Album.findByIdAndUpdate(albumId, { image: file_name }, (err, albumUpdated) =>  {
                if (err) {
                    return res.status(500).send({ message: 'Error al actualizar el album.' });
                } else {
                    if (!albumUpdated) {
                        return res.status(404).send({ message: 'User not found.' });
                    } else {
                        return res.status(200).send({ message: albumUpdated });
                    }
                }
            });
        } else {
            return res.status(200).send('Invalid image format.');
        }

        console.log(file_name);
    } else {
        return res.status(200).send({ message: "Couldn't upload file." });
    }
}

function getImageFile(req, res) {
    var imageFile = req.params.imageFile;

    var filePath = './uploads/albums/' + imageFile;

    fs.exists(filePath, function(exists) {
        if (exists) {
            return res.sendFile(path.resolve(filePath));
        } else {
            return res.status(200).send({ message: "The image doesn't exist in the system." });
        }
    });
}

module.exports = {
    getAlbum,
    getAlbums,
    saveAlbum,
    updateAlbum,
    deleteAlbum,
    uploadImage,
    getImageFile
};
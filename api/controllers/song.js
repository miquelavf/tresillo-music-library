'use strict'

var path = require('path');
var fs = require('fs');
require("mongoose-pagination");

var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

function getSong(req, res) {
    var songId = req.params.id;

    Song.findById(songId).populate({ path: 'album', populate: { path: 'artist' } }).exec((err, song) => {
        if (err) {
            return res.status(500).send({ message: 'Error en la petición.' });
        } else {
            if (!song) {
                return res.status(404).send({ message: 'El song no existe.' });
            } else {
                return res.status(200).send({ song: song });
            }
        }
    });
}

function getSongs(req, res) {
    var albumId = req.params.albumId;

    if (!albumId) {
        var find = Song.find({}).sort('number');
    } else {
        var find = Song.find({ album: albumId }).sort('number');
    }

    find.populate({ path: 'album', populate: { path: 'artist' } }).exec((err, songs) => {
        if (err) {
            return res.status(500).send({ message: 'Error en la petición.' });
        } else {
            if (!songs) {
                return res.status(404).send({ message: 'No se han encontrado canciones' });
            } else {
                return res.status(200).send({ songs: songs });
            }
        }
    });
}

function saveSong(req, res) {
    var song = new Song();

    var params = req.body;
    song.number = params.number;
    song.name = params.name;
    song.duration = params.duration;
    song.file = 'null';
    song.album = params.album;

    song.save((err, songStored) => {
        if (err) {
            return res.status(500).send({ message: 'Error en la petición.' });
        } else {
            if (!songStored) {
                return res.status(404).send({ message: 'No se ha guardado la canción.' });
            } else {
                return res.status(200).send({ song: songStored });
            }
        }
    });
}

function updateSong(req, res) {
    var songId = req.params.id;
    var update = req.body;

    Song.findByIdAndUpdate(songId, update, (err, songUpdated) => {
        if (err) {
            return res.status(500).send({ message: 'Error en la petición.' });
        } else {
            if (!songUpdated) {
                return res.status(404).send({ message: 'No se ha actualizado la canción.' });
            } else {
                return res.status(200).send({ song: songUpdated });
            }
        }
    });
}

function deleteSong(req, res) {
    var songId = req.params.id;

    Song.findByIdAndDelete(songId, (err, songDeleted) => {
        if (err) {
            return res.status(500).send({ message: 'Error en la petición.' });
        } else {
            if (!songDeleted) {
                return res.status(404).send({ message: 'No se ha actualizado la canción.' });
            } else {
                return res.status(200).send({ song: songDeleted });
            }
        }
    });
}

function uploadFile(req, res) {
    var songId = req.params.id;
    var file_name = 'Not uploaded';

    if (req.files) {
        var file_path = req.files.file.path;

        file_name = file_path.split(/(\\|\/)/g).pop();
        var file_ext = file_name.split('\.').pop();

        if (file_ext == 'mp3' || file_ext == 'ogg' || file_ext == 'wav') {
            Song.findByIdAndUpdate(songId, { file: file_name }, (err, songUploaded) =>  {
                if (err) {
                    return res.status(500).send({ message: 'Error al subir la cancion.' });
                } else {
                    if (!songUploaded) {
                        return res.status(404).send({ message: 'User not found.' });
                    } else {
                        return res.status(200).send({ song: songUploaded });
                    }
                }
            });
        } else {
            return res.status(200).send('Invalid image format.');
        }

        console.log(file_name);
    } else {
        return res.status(200).send({ message: "Couldn't upload file." });
    }
}

function getSongFile(req, res) {
    var songFile = req.params.songFile;

    var filePath = './uploads/songs/' + songFile;

    fs.exists(filePath, (exists) => {
        if (exists) {
            return res.sendFile(path.resolve(filePath));
        } else {
            return res.status(200).send({ message: "The song doesn't exist in the system." });
        }
    });
}

module.exports = {
    saveSong,
    getSong,
    getSongs,
    updateSong,
    deleteSong,
    uploadFile,
    getSongFile
};
'use strict'

var fs = require('fs');
var path = require('path');
var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');
var jwt = require('../services/jwt');

function saveUser(req, res) {
    var user = new User();
    var params = req.body;

    user.name = params.name;
    user.surname = params.surname;
    user.email = params.email;
    user.role = 'ROLE_USER';
    user.image = 'null';

    if (params.password) {
        bcrypt.hash(params.password, null, null, function (err, hash) {
            user.password = hash;
            if (user.name != null && user.surname != null && user.email != null) {
                //save user to DB
                user.save((err, userStored) => {
                    if (err) {
                        return res.status(500).send({ message: 'Error al guardar el usuario.' });
                    } else {
                        if (!userStored) {
                            return res.status(404).send({ message: 'No se ha registrado el usuario.' });
                        } else {
                            return res.status(200).send({ user: userStored });
                        }
                    }
                });
            } else {
                return res.status(400).send({ message: 'Porfavor escribe todos los campos.' });
            }
        })
    } else {
        return res.status(400).send({ message: 'Introduce la contraseña' });
    }
}

function loginUser(req, res) {
    var params = req.body;

    var email = params.email;
    var password = params.password;

    User.findOne({ email: email.toLowerCase() }, (err, user) => {
        if (err) {
            return res.status(500).send({ message: 'Error en la petición.' })
        } else {
            if (!user) {
                return res.status(500).send({ message: 'El usuario no existe.' })
            } else {
                // Check password
                bcrypt.compare(password, user.password, function (err, check) {
                    if (check) {
                        // Return logged user data
                        if (params.gethash) {
                            // Return JWT token
                            return res.status(200).send({ token: jwt.createToken(user) })
                        } else {
                            return res.status(200).send({ user: user });
                        }
                    } else {
                        return res.status(500).send({ message: 'La contraseña es incorrecta.' })
                    }
                })
            }
        }
    })
}

function updateUser(req, res) {
    var userId = req.params.id;
    var update = req.body;

    if (userId != req.user.sub) {
        return res.status(500).send({ message: 'Error al actualizar el usuario.' });
    }

    User.findByIdAndUpdate(userId, update, (err, userUpdated) => {
        if (err) {
            return res.status(500).send({ message: 'Error al actualizar el usuario.' });
        } else {
            if (!userUpdated) {
                return res.status(404).send({ message: 'User not found.' });
            } else {
                return res.status(200).send({ message: userUpdated });
            }
        }
    });
}

function uploadImage(req, res) {
    var userId = req.params.id;
    var file_name = 'Not uploaded';

    if (userId != req.user.sub) {
        return res.status(500).send({ message: 'Error al actualizar el usuario.' });
    }

    if (req.files) {
        var file_path = req.files.image.path;

        file_name = file_path.split(/(\\|\/)/g).pop();
        var file_ext = file_name.split('\.').pop();

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg') {
            User.findByIdAndUpdate(userId, { image: file_name }, (err, userUpdated) => {
                if (err) {
                    return res.status(500).send({ message: 'Error al actualizar el usuario.' });
                } else {
                    if (!userUpdated) {
                        return res.status(404).send({ message: 'User not found.' });
                    } else {
                        return res.status(200).send({ image: file_name, message: userUpdated });
                    }
                }
            });
        } else {
            return res.status(200).send('Invalid image format.');
        }

        console.log(file_name);
    } else {
        return res.status(200).send({ message: "Couldn't upload file." });
    }
}

function getImageFile(req, res) {
    var imageFile = req.params.imageFile;

    var filePath = './uploads/users/' + imageFile;

    fs.exists(filePath, function (exists) {
        if (exists) {
            return res.sendFile(path.resolve(filePath));
        } else {
            return res.status(200).send({ message: "The image doesn't exist in the system." });
        }
    });
}

module.exports = {
    saveUser,
    loginUser,
    updateUser,
    uploadImage,
    getImageFile
};